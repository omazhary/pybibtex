# parser.py

"""
This file contains functions required to translate a bibtex entry to a python
dict.
"""

import re


def clean_field(raw_string):
    clean_string = raw_string.replace('{', '')
    clean_string = clean_string.replace('}', '')
    clean_string = clean_string.replace('--', '-')
    clean_string = clean_string.replace('@', '')
    return clean_string


def parse_bibtex(input_string):
    result = dict()
    input_string = input_string.strip()
    # Pull out identifier and type
    first_line = re.match(r"@\w+{\w+", input_string).group(0)
    first_line_halves = first_line.split('{')
    result['type'] = clean_field(first_line_halves[0])
    result['identifier'] = clean_field(first_line_halves[1])
    matches = re.findall(r"\w+={.*}", input_string)
    for match in matches:
        halves = match.split('=')
        halves[1] = clean_field(halves[1])
        # There could be multiple authors
        if halves[0] == 'author' and ' and ' in halves[1]:
            halves[1] = halves[1].split(' and ')
        # Years should be treated like numbers
        if halves[0] == 'year':
            halves[1] = int(halves[1])
        result[halves[0]] = halves[1]
    return result
