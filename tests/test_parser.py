"""
This class tests the following functionality in the parser module:
- Parsing a bibtex entry to a python dict
- Parsing several entries to a list of dicts
"""

import pytest
from pybibtex.parser import parse_bibtex


def test_single_entry():
    """
    Test that it can parse a single bibtex entry
    """
    input = """
        @inproceedings{fitzgerald2014continuous,
          title={Continuous software engineering and beyond: trends and challenges},
          author={Fitzgerald, Brian and Stol, Klaas-Jan},
          booktitle={Proceedings of the 1st International Workshop on Rapid Continuous Software Engineering},
          pages={1--9},
          year={2014},
          organization={ACM}
        }
    """
    output = {
        'type': 'inproceedings',
        'identifier': 'fitzgerald2014continuous',
        'title': 'Continuous software engineering and beyond: trends and challenges',
        'author':[
            'Fitzgerald, Brian',
            'Stol, Klaas-Jan'
        ],
        'booktitle': 'Proceedings of the 1st International Workshop on Rapid Continuous Software Engineering',
        'pages': '1-9',
        'year': 2014,
        'organization': 'ACM'
    }
    assert(parse_bibtex(input) == output)


if __name__ == '__main__':
    pytest.main()
